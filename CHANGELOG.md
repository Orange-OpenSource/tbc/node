# [2.1.0](https://gitlab.com/to-be-continuous/node/compare/2.0.2...2.1.0) (2021-11-23)


### Features

* **nodejsscan:** add SARIF output format ([b95ca31](https://gitlab.com/to-be-continuous/node/commit/b95ca3108a05a892313d8b6bfd4068404b73742c)), closes [#9](https://gitlab.com/to-be-continuous/node/issues/9)

## [2.0.2](https://gitlab.com/to-be-continuous/node/compare/2.0.1...2.0.2) (2021-10-13)


### Bug Fixes

* eslint report JSON format ([64432f2](https://gitlab.com/to-be-continuous/node/commit/64432f24813341e680b5f7af74f08076d30ddede))

## [2.0.1](https://gitlab.com/to-be-continuous/node/compare/2.0.0...2.0.1) (2021-10-07)


### Bug Fixes

* use master or main for production env ([f3285e7](https://gitlab.com/to-be-continuous/node/commit/f3285e7f452e8d1ce92f38a3bc0bbdc891983d4c))

## [2.0.0](https://gitlab.com/to-be-continuous/node/compare/1.2.0...2.0.0) (2021-09-08)

### Features

* Change boolean variable behaviour ([6d4ba9d](https://gitlab.com/to-be-continuous/node/commit/6d4ba9ddc6a4cd1e670ab6dcb21f1e832894255e))

### BREAKING CHANGES

* boolean variable now triggered on explicit 'true' value

## [1.2.0](https://gitlab.com/to-be-continuous/node/compare/1.1.1...1.2.0) (2021-06-11)

### Features

* move group ([4d3219b](https://gitlab.com/to-be-continuous/node/commit/4d3219b87eb2e8eb2b09cf560bce09abb28aa027))

## [1.1.1](https://gitlab.com/Orange-OpenSource/tbc/node/compare/1.1.0...1.1.1) (2021-05-19)

### Bug Fixes

* **lint:** remove useless --force param ([347faa8](https://gitlab.com/Orange-OpenSource/tbc/node/commit/347faa83d11a1d613d1bd479ec103cea62db89ec))

## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/node/compare/1.0.0...1.1.0) (2021-05-18)

### Features

* add scoped variables support ([bc25fa0](https://gitlab.com/Orange-OpenSource/tbc/node/commit/bc25fa02e8052973b932273605ece936718f3fe3))

## 1.0.0 (2021-05-06)

### Features

* initial release ([07fdee7](https://gitlab.com/Orange-OpenSource/tbc/node/commit/07fdee71d89f3f3413a64cf6151d0bb4a0daa971))
